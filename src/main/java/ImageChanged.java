import java.awt.Color;
import java.awt.image.BufferedImage;

import org.imgscalr.Scalr;


public class ImageChanged
{
	static BufferedImage imageAfter;
	
	
	/**
 	* Funkcja do skalowania
 	* @param imageBefore - zdj�cie wej�ciowe
 	* @param method - lista warto�ci klasy Scalr.Method ( metody skalowania )
 	* @param Mode - lista warto�ci klasy Scalr.Mode ( jako�� skalowania )
 	* @param x - rz�dana szeroko��
 	* @param y - rz�dana wysoko��
 	* @return imageAfter - zdj�cie po skalowaniu
	*/
	public static BufferedImage resizeImage( BufferedImage imageBefore ,Scalr.Method method, Scalr.Mode mode,int x, int y )
	{
		if ( method == null && mode == null )
		{
			imageAfter = Scalr.resize(imageBefore, x, y);
		}
		else if ( method != null && mode != null )
		{
			imageAfter = Scalr.resize(imageBefore, method,mode, x, y);
		}
		else if ( method == null && mode != null )
		{
			imageAfter = Scalr.resize(imageBefore, mode, x, y);
		}
		else 
		{
			imageAfter = Scalr.resize(imageBefore, method, x, y);
		}
		return imageAfter;
	}
	
	/**
 	* Funkcja do rotacji
 	* @param imageBefore - zdj�cie wej�ciowe
 	* @param rot - lista warto�ci klasy Scalr.Rotation ( metody rotacji )
 	* @return imageAfter - zdj�cie po skalowaniu
	*/
	public static BufferedImage rotateImage( BufferedImage imageBefore, Scalr.Rotation rot )
	{
		imageAfter = Scalr.rotate( imageBefore, rot );
		return imageAfter;	
	}
	
	/**
 	* Funkcja do dodania ramki
 	* @param imageBefore - zdj�cie wej�ciowe
 	* @param pad - liczba pixeli grubo�ci ramki
 	* @param color - kolor ramki
 	* @return imageAfter - zdj�cie po skalowaniu
	*/
	public static BufferedImage addPadding ( BufferedImage imageBefore, int pad, Color color )
	{
		if ( color == null )
		{
			imageAfter = Scalr.pad( imageBefore, pad );
		}
		else
		{
			imageAfter = Scalr.pad( imageBefore, pad, color );
		}
		return imageAfter;
	}
	
	/**
 	* Funkcja do wycinania obszaru ze zdj�cia
 	* @param imageBefore - zdj�cie wej�ciowe
 	* @param width - szeroko�� wyci�tego obszaru
 	* @param height - wysoko�� wyci�tego obszaru
 	* @param x - wsp�rz�dna x lewego g�rnego rogu wyci�tego obszaru
 	* @param y - wsp�rz�dna y lewego g�rnego rogu wyci�tego obszaru
 	* @return imageAfter - zdj�cie po skalowaniu
	*/
	public static BufferedImage cropImage ( BufferedImage imageBefore, int width, int height, int x, int y )
	{
		imageAfter = Scalr.crop(imageBefore, width, height, x, y );
		return imageAfter;
	}
}
