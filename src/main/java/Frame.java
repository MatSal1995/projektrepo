import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.imgscalr.Scalr.Mode;
import org.imgscalr.Scalr.Rotation;

public class Frame extends JFrame implements ActionListener, MouseListener, MouseMotionListener
{
	
	private JLabel lImageIn, lImageOut, lImageInTitle, lImageOutTitle, lWidthIn, lHeightIn, lPadSize ;
	private BufferedImage imageIn,imageOut;
	private JButton bOpen, bSave, bResizeOption, bSubmitDimension, bModeOption, bMethodOption, bRotate, bSubmitRotate, bPadColor, bPadding, bSubmitPad, bReplaceImage;
	private JTextField tWidthIn, tHeightIn, tPadSize;
	private ButtonGroup bgGroupMode, bgGroupMethod, bgGroupRotate;
	private JRadioButton rbWidth, rbHeight, rbExactDemensions, rbQuality, rbSpeed, rbUltraQuality, rb90, rb180, rb270, rbVert, rbHor;
	private JCheckBox chCrop;
	private  Color color;
	private int rule = AlphaComposite.SRC_OVER;
	private float alpha = .15F;
	private Point startPoint;
	private int yChanged = 0, xCropMargin = 57, yCropMargin = 280;
	public static Icon iconIn;
	
	public Frame()  
	{	
		/**
	 	* Uworzenie okna i rozmiesczenie wszysktich przycisk�w 
		*/
		super("Image Resizer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1200, 1000);
        setVisible(true);
        setLayout(null);         
        
        
        lImageIn = new JLabel();
		add(lImageIn);
		lImageIn.setBounds(50, 250, 500, 500);		
		
		lImageInTitle = new JLabel("Edytowany obraz");
		add(lImageInTitle);
		lImageInTitle.setBounds(50, 230, 120, 20);
		
		lImageOut = new JLabel();
		add(lImageOut);
		lImageOut.setBounds(610, 250, 500, 500);
		
		lImageOutTitle = new JLabel("Obraz po edytcji");
		add(lImageOutTitle);
		lImageOutTitle.setBounds(610, 230, 120, 20);
		
		lWidthIn = new JLabel("Szeroko��: ");
		add(lWidthIn);
		lWidthIn.setBounds(50, 80, 80, 20);
		lWidthIn.setVisible(false);
		
		lHeightIn = new JLabel("Wysoko��: ");
		add(lHeightIn);
		lHeightIn.setBounds(50, 110, 80, 20);
		lHeightIn.setVisible(false);
		
		lPadSize = new JLabel("Szeroko�� obramowania: ");
		add(lPadSize);
		lPadSize.setBounds(500, 80, 150, 20);
		lPadSize.setVisible(false);
		
		
		tWidthIn = new JTextField();
		tWidthIn.setToolTipText("rozmiar podawany w pixelach");
		add(tWidthIn);
		tWidthIn.setBounds(140, 80, 50, 20);
		tWidthIn.setVisible(false);
		
		tHeightIn = new JTextField();
		tWidthIn.setToolTipText("rozmiar podawany w pixelach");
		add(tHeightIn);
		tHeightIn.setBounds(140, 110, 50, 20);
		tHeightIn.setVisible(false);
		
		tPadSize = new JTextField();
		tPadSize.setToolTipText("rozmiar podawany w pixelach");
		add(tPadSize);
		tPadSize.setBounds(660, 80, 50, 20);
		tPadSize.setVisible(false);
		
		
		bOpen = new JButton("Otw�rz obraz z pliku");
		bOpen.setBounds(50, 800, 160, 20);
		add(bOpen);
		bOpen.addActionListener(this);
		
		bSave = new JButton("Zapisz obraz do pliku");
		bSave.setBounds(610, 800, 160, 20);
		add(bSave);
		bSave.addActionListener(this);
		
		bResizeOption = new JButton("Skaluj");
		bResizeOption.setBounds(50, 50, 160, 20);
		add(bResizeOption);
		bResizeOption.addActionListener(this);	
		
		bSubmitDimension = new JButton("Zatwierd� skalowanie");
		bSubmitDimension.setBounds(50, 140, 160, 20);
		add(bSubmitDimension);
		bSubmitDimension.addActionListener(this);	
		bSubmitDimension.setVisible(false);
		
		bModeOption = new JButton("Parametry dopasowania");
		bModeOption.setBounds(50, 180, 180, 20);
		add(bModeOption);
		bModeOption.addActionListener(this);	
		bModeOption.setVisible(false);
		
		bMethodOption = new JButton("Parametry jako�ciowe");
		bMethodOption.setBounds(50, 210, 180, 20);
		add(bMethodOption);
		bMethodOption.addActionListener(this);	
		bMethodOption.setVisible(false);
		
		bRotate = new JButton("Obr��");
		bRotate.setBounds(270, 50, 160, 20);
		add(bRotate);
		bRotate.addActionListener(this);	
		
		bSubmitRotate = new JButton("Zatwierd� rotacj�");
		bSubmitRotate.setBounds(330, 140, 160, 20);
		add(bSubmitRotate);
		bSubmitRotate.addActionListener(this);	
		bSubmitRotate.setVisible(false);
		
		bPadding = new JButton("Obramowanie");
		bPadding.setBounds(510, 50, 160, 20);
		add(bPadding);
		bPadding.addActionListener(this);	
		bPadding.setVisible(true);
		
		bPadColor = new JButton("Kolor ramki");
		bPadColor.setBounds(500, 110, 120, 20);
		add(bPadColor);
		bPadColor.addActionListener(this);	
		bPadColor.setVisible(false);
		
		bSubmitPad = new JButton("Zatwierd� obramowanie");
		bSubmitPad.setBounds(500, 140, 180, 20);
		add(bSubmitPad);
		bSubmitPad.addActionListener(this);	
		bSubmitPad.setVisible(false);
		
		bReplaceImage = new JButton("<");
		bReplaceImage.setToolTipText("Przenie� uko�czone zdj�cie do ponownej edycji");
		bReplaceImage.setBounds(555, 490, 50, 40);
		add(bReplaceImage);
		bReplaceImage.addActionListener(this);
		bReplaceImage.setEnabled(false);
		
		
		JScrollPane scrollIn = new JScrollPane(lImageOut);
		scrollIn.setBounds(610, 250, 500, 500);
		add(scrollIn);
		
		JScrollPane scrollOut = new JScrollPane(lImageIn);
		scrollOut.setBounds(50, 250, 500, 500);
		add(scrollOut);
		

		bgGroupMode = new ButtonGroup();
		rbWidth = new JRadioButton("Dopasuj do szeroko�ci");
		rbHeight = new JRadioButton("Dopasuj do wysoko�ci");
		rbExactDemensions = new JRadioButton("Dok�adne wymiary");
		rbWidth.setBounds(50, 180, 160, 20);
		rbHeight.setBounds(220, 180, 160, 20);
		rbExactDemensions.setBounds(390, 180, 160, 20);
		bgGroupMode.add(rbHeight);
		bgGroupMode.add(rbWidth);
		bgGroupMode.add(rbExactDemensions);
		add(rbWidth);
		add(rbHeight);
		add(rbExactDemensions);
		rbHeight.addActionListener(this);
		rbWidth.addActionListener(this);
		rbExactDemensions.addActionListener(this);
		rbHeight.setVisible(false);
		rbWidth.setVisible(false);
		rbExactDemensions.setVisible(false);
		
		bgGroupMethod = new ButtonGroup();
		rbSpeed = new JRadioButton("Szybka");
		rbQuality = new JRadioButton("Wysoka jako��");
		rbUltraQuality = new JRadioButton("Jako�c ultra");
		rbSpeed.setBounds(50, 210, 80, 20);
		rbQuality.setBounds(140, 210, 130, 20);
		rbUltraQuality.setBounds(270, 210, 140, 20);
		bgGroupMethod.add(rbSpeed);
		bgGroupMethod.add(rbQuality);
		bgGroupMethod.add(rbUltraQuality);
		add(rbSpeed);
		add(rbQuality);
		add(rbUltraQuality);
		rbSpeed.addActionListener(this);
		rbQuality.addActionListener(this);
		rbUltraQuality.addActionListener(this);
		rbSpeed.setVisible(false);
		rbQuality.setVisible(false);
		rbUltraQuality.setVisible(false);
		
		bgGroupRotate = new ButtonGroup();
		rb90 = new JRadioButton("90 stopni");
		rb180 = new JRadioButton("180 stopni");
		rb270 = new JRadioButton("270 stopni");
		rbVert = new JRadioButton("wzgl�dem osi Y");
		rbHor = new JRadioButton("wzgl�dem osi X");
		rb90.setBounds(230, 80, 80, 20);
		rb180.setBounds(230, 110, 90, 20);
		rb270.setBounds(230, 140, 90, 20);
		rbVert.setBounds(330, 80, 130, 20);
		rbHor.setBounds(330, 110, 130, 20);
		bgGroupRotate.add(rb90);
		bgGroupRotate.add(rb180);
		bgGroupRotate.add(rb270);
		bgGroupRotate.add(rbVert);
		bgGroupRotate.add(rbHor);
		add(rb90);
		add(rb180);
		add(rb270);
		add(rbVert);
		add(rbHor);
		rb90.addActionListener(this);
		rb180.addActionListener(this);
		rb270.addActionListener(this);
		rbVert.addActionListener(this);
		rbHor.addActionListener(this);
		rb90.setVisible(false);
		rb180.setVisible(false);
		rb270.setVisible(false);
		rbVert.setVisible(false);
		rbHor.setVisible(false);	
		
		
		chCrop = new JCheckBox("wytnij");
		chCrop.setBounds(690, 50, 100, 20);
		add(chCrop);
		chCrop.addActionListener(this);
		
		JOptionPane.showMessageDialog(null, "Witaj w programie Image Resizer\n" +
		"W celu edycji zdj�cia zaimportuj zdj�cie z komputera a nast�pnie wybierz parametry zmiany zdj�cia.\n" +
		"Po wszystkim mo�esz zapisa� gotowe zdj�cie na swoim komputerze");
	}
	
	/**
 	* Klasa zagnie�d�ona ruzyj�ca prostok�t - potrzebna przy wycinaniu 
	*/
    static class Rectangle
    {
        int x1,y1,x2,y2;

        Rectangle(int _x1, int _y1,int _x2, int _y2)
        {
            x1=_x1;
            y1=_y1;
            x2=_x2;
            y2=_y2;
        }

         void paint(Graphics g)
         {
             g.drawRect(x1, y1, x2, y2);
         }
    }

	public void actionPerformed(ActionEvent e)
	{
		/**
	 	* Wczytanie zdj�cia z pliku 
		*/
		Object src = e.getSource();
		if ( src == bOpen )
		{
		 JFileChooser fc = new JFileChooser();
		 if (fc.showOpenDialog(null)==JFileChooser.APPROVE_OPTION)
		 {
			 
			 try 
			 {
				imageIn = ImageIO.read(fc.getSelectedFile());
			 } 
			 catch (IOException e1)
			 {
				e1.printStackTrace();
			 }
			 
			iconIn = new ImageIcon(imageIn);
			lImageIn.setIcon(iconIn);
			
		 }
		}
		
		/**
	 	* Zapid zdj�cia z pliku 
		*/
		else if ( src == bSave )
		{
			 JFileChooser fc = new JFileChooser();
			 if (fc.showSaveDialog(null)==JFileChooser.APPROVE_OPTION)
			 {
			  try
			  {
				ImageIO.write(imageOut, "png", fc.getSelectedFile());
			  } 
			  catch (IOException e1)
			  {
				e1.printStackTrace();
			  }	
			  
			 }
		}
		
		/**
	 	* Przycisk powoduje pokazanie si� wszystkich komponent�w zwi�zanych ze skalowaniem 
		*/
		else if ( src == bResizeOption )
		{
			if ( lWidthIn.isVisible() == true )
			{
				rbHeight.setVisible(false);
				rbWidth.setVisible(false);
				rbExactDemensions.setVisible(false);
				rbSpeed.setVisible(false);
				rbQuality.setVisible(false);
				rbUltraQuality.setVisible(false);
			}
			else
			{
			}
			lWidthIn.setVisible(true);
			lHeightIn.setVisible(true);
			tWidthIn.setVisible(true);
			tHeightIn.setVisible(true);
			bSubmitDimension.setVisible(true);
			bModeOption.setVisible(true);
			bMethodOption.setVisible(true);
		}	
		
		/**
	 	* Przycisk uruchamiaj�cy skalowanie zdj�cia za pomoc� metody ImageChanged.resizeImage
	 	* @param met - paramtr listy warto�ci z klasy Scalr.Method
	 	* @param mod - lista warto�ci z klasy Scalr.Mode
		*/
		else if ( src == bSubmitDimension )
		{
			Scalr.Method met = null;
			Scalr.Mode mod = null;			
			try
			{
				if ( rbHeight.isSelected() == true )
				{
					if ( rbSpeed.isSelected() == true )
					{
						met = Method.SPEED;
						mod = Mode.FIT_TO_HEIGHT;
					}
					else if (rbQuality.isSelected() == true )
					{
						met = Method.QUALITY;
						mod = Mode.FIT_TO_HEIGHT;
					}
					else if (rbUltraQuality.isSelected() == true )
					{
						met = Method.ULTRA_QUALITY;
						mod = Mode.FIT_TO_HEIGHT;
					}
					else
					{
						mod = Mode.FIT_TO_HEIGHT;
					}
				}
				else if ( rbWidth.isSelected() == true )
				{
					if ( rbSpeed.isSelected() == true )
					{
						met = Method.SPEED;
						mod = Mode.FIT_TO_WIDTH;
					}
					else if (rbQuality.isSelected() == true )
					{
						met = Method.QUALITY;
						mod = Mode.FIT_TO_WIDTH;
					}
					else if (rbUltraQuality.isSelected() == true )
					{
						met = Method.ULTRA_QUALITY;
						mod = Mode.FIT_TO_WIDTH;
					}
					else
					{
						mod = Mode.FIT_TO_WIDTH;
					}
				}
				else if ( rbExactDemensions.isSelected() == true )
				{
					if ( rbSpeed.isSelected() == true )
					{
						met = Method.SPEED;
						mod = Mode.FIT_EXACT;
					}
					else if (rbQuality.isSelected() == true )
					{
						met = Method.QUALITY;
						mod = Mode.FIT_EXACT;
					}
					else if (rbUltraQuality.isSelected() == true )
					{
						met = Method.ULTRA_QUALITY;
						mod = Mode.FIT_EXACT;
					}
					else
					{
						mod = Mode.FIT_EXACT;
					}
				}
				else if ( rbSpeed.isSelected() == true )
				{
					met = Method.SPEED;
				}
				else if ( rbQuality.isSelected() == true )
				{
					met = Method.QUALITY;
				}
				else if ( rbUltraQuality.isSelected() == true )
				{
					met = Method.ULTRA_QUALITY;
				}
				
				imageOut = ImageChanged.resizeImage( imageIn, met, mod ,Integer.parseInt(tWidthIn.getText()), Integer.parseInt(tHeightIn.getText()) );
				
				ImageIcon iconOut = new ImageIcon(imageOut);
				lImageOut.setIcon(iconOut);
				lImageOut.setVisible(true);
				
				JOptionPane.showMessageDialog(null, "szer: " +  imageOut.getWidth() + ", wys: "+ imageOut.getHeight());
				
				rbHeight.setVisible(false);
				rbWidth.setVisible(false);
				rbExactDemensions.setVisible(false);
				lWidthIn.setVisible(false);
				lHeightIn.setVisible(false);
				tWidthIn.setVisible(false);
				tHeightIn.setVisible(false);
				bSubmitDimension.setVisible(false);
				bModeOption.setVisible(false);
				rbSpeed.setVisible(false);
				rbQuality.setVisible(false);
				rbUltraQuality.setVisible(false);
				bMethodOption.setVisible(false);
				bgGroupMode.clearSelection();
				bgGroupMethod.clearSelection();
				bReplaceImage.setEnabled(true);

			}
			catch (NumberFormatException e1)
			{
				JOptionPane.showMessageDialog(null, "Prosz� poda� warto�ci liczbowe");
				rbHeight.setVisible(false);
				rbWidth.setVisible(false);
				rbExactDemensions.setVisible(false);
				bModeOption.setVisible(true);
				rbSpeed.setVisible(false);
				rbQuality.setVisible(false);
				rbUltraQuality.setVisible(false);
				bMethodOption.setVisible(true);
				bgGroupMode.clearSelection();
				bgGroupMethod.clearSelection();
			}
			catch (IllegalArgumentException e1)
			{
				JOptionPane.showMessageDialog(null, "Prosz� wprowadzi� zdj�cie");
				rbHeight.setVisible(false);
				rbWidth.setVisible(false);
				rbExactDemensions.setVisible(false);
				bModeOption.setVisible(true);
				rbSpeed.setVisible(false);
				rbQuality.setVisible(false);
				rbUltraQuality.setVisible(false);
				bMethodOption.setVisible(true);
				bgGroupMode.clearSelection();
				bgGroupMethod.clearSelection();
			}				
		}
		
		/**
	 	* Schownie przycisk�w aby nie nak�ada�y si� na siebie
		*/
		else if ( src == bModeOption )
		{
			bModeOption.setVisible(false);
			rbHeight.setVisible(true);
			rbWidth.setVisible(true);
			rbExactDemensions.setVisible(true);
		}
		else if ( src == bMethodOption )
		{
			bMethodOption.setVisible(false);
			rbSpeed.setVisible(true);
			rbQuality.setVisible(true);
			rbUltraQuality.setVisible(true);
		}
		
		/**
	 	* Przycisk powoduje pokazanie si� wszystkich komponent�w zwi�zanych z rotacj� 
		*/
		else if ( src == bRotate )
		{
			rb90.setVisible(true);
			rb180.setVisible(true);
			rb270.setVisible(true);
			rbVert.setVisible(true);
			rbHor.setVisible(true);
			bSubmitRotate.setVisible(true);
		}
		
		/**
	 	* Przycisk uruchamiaj�cy rotacje zdj�cia za pomoc� metody ImageChanged.rotateImage
	 	* @param rot - paramtr listy warto�ci z klasy Scalr.Rotation
		*/
		else if ( src == bSubmitRotate )
		{	
			try
			{
				Scalr.Rotation rot = null;
				
				if ( rb90.isSelected() == true )
				{
					rot = Rotation.CW_90;
				}
				else if ( rb180.isSelected() == true )
				{
					rot = Rotation.CW_180;
				}
				else if ( rb270.isSelected() == true )
				{
					rot = Rotation.CW_270;
				}
				else if ( rbVert.isSelected() == true )
				{
					rot = Rotation.FLIP_VERT;
				}
				else if ( rbHor.isSelected() == true )
				{
					rot = Rotation.FLIP_HORZ;
				}

				if ( rot != null )
				{
					imageOut = ImageChanged.rotateImage(imageIn, rot );
					ImageIcon iconOut = new ImageIcon(imageOut);
					lImageOut.setIcon(iconOut);
					lImageOut.setVisible(true);
					bReplaceImage.setEnabled(true);
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Prosz� zaznaczy� wybran� metod� obrotu");
				}
									
				rb90.setVisible(false);
				rb180.setVisible(false);
				rb270.setVisible(false);
				rbVert.setVisible(false);
				rbHor.setVisible(false);
				bSubmitRotate.setVisible(false);
				bgGroupRotate.clearSelection();
			}
			catch (IllegalArgumentException e1 )
			{
				JOptionPane.showMessageDialog(null, "Prosz� wprowadzi� zdj�cie");
				rb90.setVisible(false);
				rb180.setVisible(false);
				rb270.setVisible(false);
				rbVert.setVisible(false);
				rbHor.setVisible(false);
				bSubmitRotate.setVisible(false);
				bgGroupRotate.clearSelection();

			}
			catch ( NullPointerException e1 )
			{
				rb90.setVisible(false);
				rb180.setVisible(false);
				rb270.setVisible(false);
				rbVert.setVisible(false);
				rbHor.setVisible(false);
				bSubmitRotate.setVisible(false);
				bgGroupRotate.clearSelection();

			}
		}
		
		/**
	 	* Przycisk powoduje pokazanie si� wszystkich komponent�w zwi�zanych z obramowaniem 
		*/
		else if ( src == bPadding )
		{
			lPadSize.setVisible(true);
			tPadSize.setVisible(true);
			bPadColor.setVisible(true);
			bSubmitPad.setVisible(true);
			
		}
		
		/**
	 	* Przycisk uruchamiaj�cy obramowanie zdj�cia za pomoc� metody ImageChanged.addPadding
		*/
		else if ( src == bSubmitPad )
		{
			try
			{			
				imageOut = ImageChanged.addPadding(imageIn, Integer.parseInt(tPadSize.getText()), color);
				ImageIcon iconOut = new ImageIcon(imageOut);
				lImageOut.setIcon(iconOut);
				lImageOut.setVisible(true);
				
				lPadSize.setVisible(false);
				tPadSize.setVisible(false);
				bPadColor.setVisible(false);
				bSubmitPad.setVisible(false);
				bReplaceImage.setEnabled(true);

			}
			catch ( NumberFormatException e1 )
			{
				JOptionPane.showMessageDialog(null, "Prosz� wprowadzi� szeroko�� obramowania w pixelach");
				lPadSize.setVisible(false);
				tPadSize.setVisible(false);
				bPadColor.setVisible(false);
				bSubmitPad.setVisible(false);
			}
			catch ( IllegalArgumentException e1 )
			{
				JOptionPane.showMessageDialog(null, "Prosz� wprowadzi� zdj�cie");
				lPadSize.setVisible(false);
				tPadSize.setVisible(false);
				bPadColor.setVisible(false);
				bSubmitPad.setVisible(false);
			}
		}
		else if ( src == bPadColor )
		{
				color = JColorChooser.showDialog(null, "kolor ramki", Color.black);
		}
		/**
	 	* Przycisk uruchamiaj�cy nas�uch mouse listener�w nad obrazemdo edytcji
		*/
		else if ( src == chCrop )
		{
			if ( chCrop.isSelected() == true )
			{
				lImageIn.addMouseListener(this);
				lImageIn.addMouseMotionListener(this); 
			}
			else
			{
				lImageIn.removeMouseListener(this);
				lImageIn.removeMouseMotionListener(this); 
			}
		}
		
		/**
	 	* Przycisk podmieniaj�cy zdj�cie wej�ciowe przez zdj�cie po edytcji
		*/
		else if  ( src == bReplaceImage )
		{
			bReplaceImage.setEnabled(false);
			iconIn = new ImageIcon(imageOut);
			imageIn = imageOut;
			lImageOut.setVisible(false);
			lImageIn.setIcon(iconIn);			
		}
	}

	public void mouseClicked(MouseEvent e) {

    }

	public void mouseEntered(MouseEvent arg0)
	{
		
	}
	public void mouseExited(MouseEvent arg0)
	{

	}
	
	/**
 	* Rozpocz�cie rysowania prostok�ta po klikni�ciu myszk� nad obszarem zdj�cia wej�ciowego
	*/
	public void mousePressed(MouseEvent e)
	{	
		startPoint = e.getPoint();
		Graphics2D g2 = (Graphics2D)getGraphics();
		
		Rectangle2D prostokat = new Rectangle2D.Double();
		prostokat.setFrame(startPoint.x, startPoint.y,0, 0);
		g2.setComposite(AlphaComposite.getInstance(rule, alpha));
		g2.draw(prostokat);
		g2.setColor(Color.BLUE);
		g2.fill(prostokat);		
	}
	public void mouseReleased(MouseEvent e) 
	{
		repaint();	
	}
	
	/**
 	* Rysowania prostok�ta rozci�gaj�cego si� ponad zdj�ciem wej�ciowym przy poruszaniu mysz� 
 	* Wyci�cie narysowanego obszaru i wstawienie w zdj�cie po edycji
	*/
	public void mouseDragged(MouseEvent e)
	{	
		try
		{
		Graphics2D g2 = (Graphics2D)getGraphics();
		Rectangle2D prostokat = new Rectangle2D.Double();
		
		prostokat.setFrame(startPoint.x + xCropMargin, startPoint.y + yCropMargin, e.getX() -  startPoint.x, e.getY() -  startPoint.y);
		g2.setComposite(AlphaComposite.getInstance(rule, alpha));
		g2.draw(prostokat);
		g2.setColor(Color.BLUE);
		g2.fill(prostokat);
		paint(g2);	
		this.revalidate();		
		
		if ( iconIn.getIconHeight() < lImageIn.getHeight() )
		{
			yChanged = (lImageIn.getHeight() - iconIn.getIconHeight() )/ 2;
		}
		
		imageOut = ImageChanged.cropImage(imageIn , startPoint.x , startPoint.y  - yChanged, e.getX() -  startPoint.x, e.getY() -  startPoint.y );
		ImageIcon iconOut = new ImageIcon(imageOut); 
		lImageOut.setIcon(iconOut);
		lImageOut.setVisible(true);
		bReplaceImage.setEnabled(true);		
		}
		catch (IllegalArgumentException e1 )
		{
			
		}		
	}
	public void mouseMoved(MouseEvent e) 
	{

	}
}


